package de.fsdev.myfoursquare

object Constants {
    const val FOURSQUARE_BASE_URL = "https://api.foursquare.com/v2/"
    const val CLIENT_ID :String = "" // insert your client id
    const val CLIENT_SECRET :String = "" // insert your client secret
    const val INITIAL_LAT_LONG :String = "" // insert any latitude/longitude, eg. "40.7099,-73.9622"
}

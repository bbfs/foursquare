package de.fsdev.myfoursquare.presentation.mainPage

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import de.fsdev.myfoursquare.Constants
import de.fsdev.myfoursquare.R
import de.fsdev.myfoursquare.network.ApiService
import de.fsdev.myfoursquare.network.data.Venue
import de.fsdev.myfoursquare.presentation.detailPage.DetailActivity
import de.fsdev.myfoursquare.utils.UtilMethods
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val venueList: ArrayList<Venue> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        venuesApiCall("dummy", this)
    }

    @SuppressLint("CheckResult")
    private fun venuesApiCall(userID: String, context: Context) {
        if (UtilMethods.isConnectedToInternet(context)) {
            UtilMethods.showLoading(context)

            val observable = ApiService.venueApiCall().getVenues(
                Constants.CLIENT_ID,
                Constants.CLIENT_SECRET,
                20191118,
                Constants.INITIAL_LAT_LONG,
                "browse",
                500,
                50)

            observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { rootVenueSearch ->
                        UtilMethods.hideLoading()

                        rootVenueSearch.response?.venues?.let {
                            drawVenues(it)
                        }
                    },
                    { error ->
                        UtilMethods.hideLoading()
                        UtilMethods.showLongToast(context, error.message.toString())
                    }
                )
        }
        else {
            UtilMethods.showLongToast(context, "No Internet Connection!")
        }
    }

    private fun drawVenues(venues: List<Venue>) {
        Log.d("MainActivity", "Number of venues: "+venues.size)
        venueList.clear()
        venueList.addAll(venues)
        val adapter =
            VenueListAdapter(
                venueList
            )

        adapter.setOnItemClickListener(object : VenueListAdapter.ClickListener {
            override fun onClick(pos: Int, aView: View) {
                Log.d("onClick", "pos: "+pos.toString() +", venue: "+venueList[pos].name)
                openDetailPage(venueList[pos].id)
            }
        })

        venue_list?.adapter = adapter
    }

    private fun openDetailPage(venueId : String?) {
        if (venueId != null) {
            val intent = Intent(this, DetailActivity::class.java)
            intent.putExtra(DetailActivity.VENUE_ID, venueId)
            startActivity(intent)
        }
    }
}

package de.fsdev.myfoursquare.network.service

import de.fsdev.myfoursquare.network.data.RootVenueSearch
import retrofit2.http.GET
import retrofit2.http.Headers
import io.reactivex.Observable
import retrofit2.http.Query

interface VenueSearchService {

    @Headers("Content-Type: application/json")
    @GET("venues/search")
    fun getVenues(
            @Query("client_id") client_id: String,
            @Query("client_secret") client_secret: String,
            @Query("v") v: Int,
            @Query("ll") ll: String,
            @Query("intent") intent: String,
            @Query("radius") radius: Int,
            @Query("limit") limit: Int
    ): Observable<RootVenueSearch>
}
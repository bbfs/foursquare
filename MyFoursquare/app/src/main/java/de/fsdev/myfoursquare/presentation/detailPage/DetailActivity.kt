package de.fsdev.myfoursquare.presentation.detailPage


import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ShareCompat
import de.fsdev.myfoursquare.Constants
import de.fsdev.myfoursquare.R
import de.fsdev.myfoursquare.network.ApiService
import de.fsdev.myfoursquare.network.data.DetailVenue
import de.fsdev.myfoursquare.utils.UtilMethods
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_detail.*


class DetailActivity : AppCompatActivity() {

    companion object {
        val VENUE_ID = "VENUE_ID_PARAM"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        intent.extras?.getString(VENUE_ID)?.let {
            venuesDetailApiCall(it, this)
        }
    }

    @SuppressLint("CheckResult")
    private fun venuesDetailApiCall(venueId: String, context: Context) {
        if (UtilMethods.isConnectedToInternet(context)) {
            UtilMethods.showLoading(context)

            val observable = ApiService.venueDetailApiCall().getDetails(
                venueId,
                Constants.CLIENT_ID,
                Constants.CLIENT_SECRET,
                20191118)

            observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { rootVenueDetail ->
                        UtilMethods.hideLoading()
                        rootVenueDetail.response?.venue?.let {
                            Log.d("DetailActivity", "OK")
                            drawDetails(it)
                        }
                    },
                    { error ->
                        UtilMethods.hideLoading()
                        UtilMethods.showLongToast(context, error.message.toString())
                    }
                )
        }
        else {
            UtilMethods.showLongToast(context, "No Internet Connection!")
        }
    }

    private fun drawDetails(details: DetailVenue) {
        detail_title?.text = details.name

        details_button_share?.setOnClickListener {
            Log.d("DetailActivity", "cannonicalUrl: "+details.canonicalUrl)
            shareLocation(details.name, details.canonicalUrl)
        }

        details_button_show_map?.setOnClickListener{
            Log.d("DetailActivity", "map location: "+details.location.address)
            val gmmIntentUri: Uri = Uri.parse("geo:"+details.location.lat+","+details.location.lng+"?z=19")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }
    }

    private fun shareLocation(locationname: String, locationUrl: String) {
        val shareIntent: Intent = ShareCompat.IntentBuilder.from(this)
            .setChooserTitle(R.string.share_location_title)
            .setType("text/plain")
            .setSubject("Location: "+locationname)
            .setText(locationUrl)
            .createChooserIntent()
        if (shareIntent.resolveActivity(packageManager) != null) {
            startActivity(shareIntent)
        }
    }
}

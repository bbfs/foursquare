package de.fsdev.myfoursquare.network.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class RootVenueSearch {
    @SerializedName("meta")
    @Expose
    var meta: Meta? = null
    @SerializedName("response")
    @Expose
    var response: Response? = null
}

class Category {
    @SerializedName("id")
    @Expose
    var id: String? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("pluralName")
    @Expose
    var pluralName: String? = null
    @SerializedName("shortName")
    @Expose
    var shortName: String? = null
    @SerializedName("icon")
    @Expose
    var icon: Icon? = null
    @SerializedName("primary")
    @Expose
    var primary: Boolean? = null
}

class Icon {
    @SerializedName("prefix")
    @Expose
    var prefix: String? = null
    @SerializedName("suffix")
    @Expose
    var suffix: String? = null
}

class LabeledLatLng {
    @SerializedName("label")
    @Expose
    var label: String? = null
    @SerializedName("lat")
    @Expose
    var lat: Double? = null
    @SerializedName("lng")
    @Expose
    var lng: Double? = null
}

class Location {
    @SerializedName("address")
    @Expose
    var address: String? = null
    @SerializedName("lat")
    @Expose
    var lat: Double? = null
    @SerializedName("lng")
    @Expose
    var lng: Double? = null
    @SerializedName("labeledLatLngs")
    @Expose
    var labeledLatLngs: List<LabeledLatLng>? = null
    @SerializedName("distance")
    @Expose
    var distance: Int? = null
    @SerializedName("postalCode")
    @Expose
    var postalCode: String? = null
    @SerializedName("cc")
    @Expose
    var cc: String? = null
    @SerializedName("city")
    @Expose
    var city: String? = null
    @SerializedName("state")
    @Expose
    var state: String? = null
    @SerializedName("country")
    @Expose
    var country: String? = null
    @SerializedName("formattedAddress")
    @Expose
    var formattedAddress: List<String>? = null
}

class Meta {
    @SerializedName("code")
    @Expose
    var code: Int? = null
    @SerializedName("requestId")
    @Expose
    var requestId: String? = null
}

class Response {
    @SerializedName("venues")
    @Expose
    var venues: List<Venue>? = null
}

class Venue {
    @SerializedName("id")
    @Expose
    var id: String? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("location")
    @Expose
    var location: Location? = null
    @SerializedName("categories")
    @Expose
    var categories: List<Category>? = null
    @SerializedName("referralId")
    @Expose
    var referralId: String? = null
    @SerializedName("hasPerk")
    @Expose
    var hasPerk: Boolean? = null
    @SerializedName("venuePage")
    @Expose
    var venuePage: VenuePage? = null
}

class VenuePage {
    @SerializedName("id")
    @Expose
    var id: String? = null
}







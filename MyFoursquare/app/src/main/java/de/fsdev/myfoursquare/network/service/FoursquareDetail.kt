package de.fsdev.myfoursquare.network.service

import de.fsdev.myfoursquare.network.data.RootVenueDetail
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface VenueDetailService {

    @Headers("Content-Type: application/json")
    @GET("venues/{venueId}")
    fun getDetails(
            @Path("venueId") venueId : String,
            @Query("client_id") client_id: String,
            @Query("client_secret") client_secret: String,
            @Query("v") v: Int
    ): Observable<RootVenueDetail>
}
package de.fsdev.myfoursquare.presentation.mainPage

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import de.fsdev.myfoursquare.R
import de.fsdev.myfoursquare.network.data.Venue
import kotlinx.android.synthetic.main.venue_list_item.view.*

class VenueListAdapter (val items : ArrayList<Venue>) : RecyclerView.Adapter<VenueListAdapter.MyViewHolder>() {

    lateinit var mClickListener: ClickListener

    fun setOnItemClickListener(aClickListener: ClickListener) {
        mClickListener = aClickListener
    }

    interface ClickListener {
        fun onClick(pos: Int, aView: View)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.venue_list_item, parent, false))
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val venue = items.get(position)

        holder?.venueName?.text = venue.name

        venue.categories?.let { list ->
            if (!list.isEmpty()) {
                list.get(0)?.name?.let {
                    holder?.venueCategory?.text = it
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class MyViewHolder (view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            mClickListener.onClick(adapterPosition, v)
        }

        val venueName = view.venueName
        val venueCategory = view.venueCategory
    }
}


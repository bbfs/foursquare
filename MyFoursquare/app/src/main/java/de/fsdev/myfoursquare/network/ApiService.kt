package de.fsdev.myfoursquare.network


import de.fsdev.myfoursquare.Constants
import de.fsdev.myfoursquare.network.service.VenueDetailService
import de.fsdev.myfoursquare.network.service.VenueSearchService
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory


object ApiService {

    fun venueApiCall() = Retrofit.Builder()
            .baseUrl(Constants.FOURSQUARE_BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(ApiWorker.gsonConverter)
            .client(ApiWorker.client)
            .build()
            .create(VenueSearchService::class.java)!!

    fun venueDetailApiCall() = Retrofit.Builder()
        .baseUrl(Constants.FOURSQUARE_BASE_URL)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(ApiWorker.gsonConverter)
        .client(ApiWorker.client)
        .build()
        .create(VenueDetailService::class.java)!!
}
package de.fsdev.myfoursquare.network.data

data class RootVenueDetail(
    val meta: DetailMeta,
    val response: DetailResponse
)

data class DetailMeta(
    val code: Int,
    val requestId: String
)

data class DetailResponse(
    val venue: DetailVenue
)

data class DetailVenue(
    val attributes: Attributes,
    val beenHere: BeenHere,
    val bestPhoto: BestPhoto,
    val canonicalUrl: String,
    val categories: List<DetailCategory>,
    val colors: Colors,
    val contact: Contact,
    val createdAt: Int,
    val dislike: Boolean,
    val hereNow: HereNow,
    val id: String,
    val inbox: Inbox,
    val likes: Likes,
    val listed: Listed,
    val location: DetailLocation,
    val name: String,
    val ok: Boolean,
    val pageUpdates: PageUpdates,
    val photos: Photos,
    val popular: Popular,
    val reasons: Reasons,
    val shortUrl: String,
    val specials: Specials,
    val stats: Stats,
    val timeZone: String,
    val tips: Tips,
    val venueRatingBlacklisted: Boolean,
    val verified: Boolean
)

data class Attributes(
    val groups: List<Any>
)

data class BeenHere(
    val count: Int,
    val lastCheckinExpiredAt: Int,
    val marked: Boolean,
    val unconfirmedCount: Int
)

data class BestPhoto(
    val createdAt: Int,
    val height: Int,
    val id: String,
    val prefix: String,
    val source: Source,
    val suffix: String,
    val visibility: String,
    val width: Int
)

data class Source(
    val name: String,
    val url: String
)

data class DetailCategory(
    val icon: DetailIcon,
    val id: String,
    val name: String,
    val pluralName: String,
    val primary: Boolean,
    val shortName: String
)

data class DetailIcon(
    val prefix: String,
    val suffix: String
)

data class Colors(
    val algoVersion: Int,
    val highlightColor: HighlightColor,
    val highlightTextColor: HighlightTextColor
)

data class HighlightColor(
    val photoId: String,
    val value: Int
)

data class HighlightTextColor(
    val photoId: String,
    val value: Int
)

data class Contact(
    val formattedPhone: String,
    val phone: String,
    val twitter: String
)

data class HereNow(
    val count: Int,
    val groups: List<Any>,
    val summary: String
)

data class Inbox(
    val count: Int,
    val items: List<Any>
)

data class Likes(
    val count: Int,
    val groups: List<Group>,
    val summary: String
)

data class Group(
    val count: Int,
    val items: List<Item>,
    val type: String
)

data class Item(
    val firstName: String,
    val gender: String,
    val id: String,
    val lastName: String,
    val photo: Photo
)

data class Photo(
    val default: Boolean,
    val prefix: String,
    val suffix: String
)

data class Listed(
    val count: Int,
    val groups: List<GroupX>
)

data class GroupX(
    val count: Int,
    val items: List<Any>,
    val name: String,
    val type: String
)

data class DetailLocation(
    val address: String,
    val cc: String,
    val city: String,
    val country: String,
    val formattedAddress: List<String>,
    val labeledLatLngs: List<DetailLabeledLatLng>,
    val lat: Double,
    val lng: Double,
    val postalCode: String,
    val state: String
)

data class DetailLabeledLatLng(
    val label: String,
    val lat: Double,
    val lng: Double
)

data class PageUpdates(
    val count: Int,
    val items: List<Any>
)

data class Photos(
    val count: Int,
    val groups: List<GroupXX>
)

data class GroupXX(
    val count: Int,
    val items: List<ItemX>,
    val name: String,
    val type: String
)

data class ItemX(
    val createdAt: Int,
    val height: Int,
    val id: String,
    val prefix: String,
    val source: SourceX,
    val suffix: String,
    val user: User,
    val visibility: String,
    val width: Int
)

data class SourceX(
    val name: String,
    val url: String
)

data class User(
    val firstName: String,
    val gender: String,
    val id: String,
    val lastName: String,
    val photo: PhotoX
)

data class PhotoX(
    val prefix: String,
    val suffix: String
)

data class Popular(
    val isLocalHoliday: Boolean,
    val isOpen: Boolean,
    val timeframes: List<Timeframe>
)

data class Timeframe(
    val days: String,
    val includesToday: Boolean,
    val `open`: List<Open>,
    val segments: List<Any>
)

data class Open(
    val renderedTime: String
)

data class Reasons(
    val count: Int,
    val items: List<Any>
)

data class Specials(
    val count: Int,
    val items: List<Any>
)

data class Stats(
    val tipCount: Int
)

data class Tips(
    val count: Int,
    val groups: List<GroupXXX>
)

data class GroupXXX(
    val count: Int,
    val items: List<Any>,
    val name: String,
    val type: String
)